## Ex<sup>4</sup>ploration

Ex<sup>4</sup>ploration : on pourrait le lire « Ex-puissance-quatre-ploration » mais, pour simplifier, on dira « Exaploration ».

Mais qu'est-ce que ça signifie? Il s'agit de l'exploration « X<sup>4</sup> », soit sur quatre dimensions, celles que nous connaissons : les trois dimensions spatiales, et la dimension temporelle. Mais point de machine à voyager dans le temps à la H.G. Wells ici, c'est un peu différent, vous verrez.

Tenez, au fait, connaissez-vous le paradoxe de Fermi? Celui qui pose ce problème : étant donné le gigantisme de notre galaxie, sa multitude d'étoiles souvent plus âgées que le Soleil, avec leur cortège de planètes, étant donnée la quantité de zones habitables statistiquement possibles ailleurs que sur Terre... Comment se fait-il que nous n'ayons encore jamais été « visités » par une civilisation extra-terrestre? Ce paradoxe suscite bien des débats, menant certains à croire en l'existence d'un « Grand Filtre » qui pour une raison ou une autre empêcherait les civilisations d'accéder à l'ère de la colonisation spatiale.

Niaiseries que tout cela, pure science-fiction. Les civilisations galactiques, extra-terrestres, existent bel et bien. Depuis bien longtemps, elles ont quitté leur berceau. Et colonisé la galaxie, tout autour de nous. Enfin, « autour », spatialement parlant.

Vous connaissez aussi Albert Einstein, évidemment. Parmi ses remarquables théories figure la notion de dilatation temporelle liée à la relativité restreinte. Une expérience, certes fictive, permet de la décrire : prenons deux jumeaux nés sur Terre. L'un d'eux part en voyage, disons vers une autre étoile, à une vitesse proche de celle de la lumière, puis revient voir son frère. Du fait de la dilatation temporelle qui s'accroît avec la vitesse, le temps sera passé moins vite pour celui ayant voyagé que pour celui resté sur Terre. Le premier retrouvera donc son frère sédentaire plus agé que lui.

Bien sûr, pour Einstein, la vitesse de la lumière représentait une limite infranchissable, une singularité. Comme souvent, les singularités en physique témoignent surtout d'une vision encore parcellaire des choses. Une recontextualisation du problème, souvent à la lumière de nouvelles découvertes, permet de replacer des équations dans un ensemble plus global faisant sauter les limites que l'on pensait infranchissables. Et de fait, la vitesse de la lumière n'est pas une limite. Les vitesses supraluminiques mises en jeu dans les déplacements interstellaires envoyaient, et envoient toujours les voyageurs non seulement vers leur destination spatiale, mais aussi temporelle : dans le futur. Loin dans le futur, à force d'errances et de zigzags spatiaux. Les extra-terrestres sont bien là autour de nous, mais pas maintenant. Plus tard, bien plus tard. Nous pourrions les rejoindre, bien sûr. Mais ce genre de voyage est sans retour.

Voilà donc ce qu'est Ex<sup>4</sup>ploration : une grande fresque des aventures extra-terrestres de l'humanité. Mais avant cela, voyons comment tout a commencé.

## Prélude

[À venir]

## Interlude

Petite pause, et relativisons. La « grande fresque » est aujourd'hui bien plus une friche qu'une fresque. Une friche ouverte, qui se veut collaborative. [Voir ici](CONTRIBUTION.md) pour plus d'infos.

## Licence

Question [licence](LICENSE), le tout est publié en [Creative Commons BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/), ce qui signifie dans les grandes lignes : partage / reproduction / modification autorisés mais crédit de(s) auteur(s) requis, pas d'utilisation commerciale.
