## VSCode config

### keybindings.json

```json
{
    "key": "alt+6",
    "command": "type",
    "args": {
        "text": "–"
    }
},
{
    "key": "alt+a",
    "command": "type",
    "args": {
        "text": "ã"
    }
},
{
    "key": "alt+3",
    "command": "type",
    "args": {
        "text": "« "
    }
},
{
    "key": "alt+shift+3",
    "command": "type",
    "args": {
        "text": " »"
    }
}
```
