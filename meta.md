## Meta-données

- **Auteur**: nom ou pseudonyme, lien vers le compte gitlab
- **Contact auteur**: méthode(s) publique(s) préférée(s) pour contacter l'auteur (mail publique, handle twitter, compte sur un forum...)
- **Repère temporel**: intervalle de temps sur lequel se déroule l'histoire. An 0 = premier voyage interstellaire (voir prélude)
- **Repère spatial**: étoiles / planètes / secteurs galactiques où se déroule l'action. Les noms peuvent être réels ou inventés.
- **Titre**: titre de l'histoire

## Éléments clés

Cette section décrit les éléments et événements clés qui ont lieu au cours de cette histoire et qui peuvent impacter le récit global, et donc les autres histoires.
Elle doit permettre aux autres auteurs de s'y retrouver plus facilement dans la fresque globale, sans devoir relire l'intégralité de chaque histoire.
