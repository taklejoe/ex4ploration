Si vous êtes auteur ou souhaitez le devenir, si cette amorce d'univers vous plaît, si vous souhaitez y laisser une trace ou impulser une direction, tout cela est possible.

## Comment faire?

Écrivez votre histoire qui prend place dans cet univers, en [markdown](https://fr.wikipedia.org/wiki/Markdown). Question taille, il faut tabler sur un format nouvelle / novella voire roman court. Pour le style de narration, pas de contrainte. Réservez un slot spatial et temporel : par exemple, "Années 3856 à 3879, Secteurs Altaïr - Aldébaran" (vous pouvez inventer des noms). Créez un répertoire avec ce nom et mettez-y votre texte, accompagné d'un fichier `meta.md` contenant des informations annexes (copiez [celui-ci](https://gitlab.com/taklejoe/ex4ploration/raw/playground/meta.md) puis éditez-le). Pour rappel, Ex<sup>4</sup>ploration est sous licence Creative Commons BY-NC-SA (non commerciale).

Par ailleurs, rien n'empêche de proposer également des modifications aux textes déjà publiés.

Ensuite, deux cas de figure :

- Si vous connaissez déjà GIT ou que ça ne vous fait pas peur d'apprendre, vous devez avoir un compte sur [gitlab](https://gitlab.com/), [forker](https://gitlab.com/taklejoe/ex4ploration/forks/new) le dépôt Ex<sup>4</sup>ploration, ajouter votre texte puis ouvrir une *merge request*.

- Sinon, ahem, contactez-moi / ouvrez une [issue](https://gitlab.com/taklejoe/ex4ploration/issues) et on trouvera une solution.

## Pourquoi GIT?

En informatique, [GIT](https://fr.wikipedia.org/wiki/Git) est devenu la norme pour synchroniser du travail collaboratif (exemple : pour le développement de logiciel). Des sites comme gitlab ou github sont des plateformes qui non seulement offrent l'hébergement des dépôts GIT, mais en plus fournissent une interface complète pour permettre la revue de code / texte, des *issues* qui peuvent servir *in fine* de forum, un Wiki, etc.

## Donc toute contribution est acceptée?

Non, pas forcément. Déjà, il y a une phase de relecture à passer, pour corriger les typos notamment. Il faut garder une cohérence vis-à-vis de l'ensemble. On est dans la SF space-opera, il faut aussi un minimum de crédibilité (sans pour autant renier toute fantaisie).

À noter qu'il sera aussi possible, à terme, de créer des "branches" scénaristiques, par exemple si certains textes engendrent des réalités mutuellement exclusives.

Mais dans l'idée, à partir du moment où une contribution colle au thème et se veut de bonne foi, je suppose que rien n'empêchera de l'intégrer.
